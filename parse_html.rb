#!/usr/bin/env ruby
require 'csv'
require 'json'
require 'nokogiri'


# Candidatos listados na página do TSE: http://divulgacandcontas.tse.jus.br/divulga/#/municipios/2018/2022802018/BR/candidatos
#
# INPUT: um arquivo texto que contém em cada linha 3 caminhos para os arquivos das informações do candidato. Os caminhos devem ser separados por vírgula
#   Formato: <página principal do candidato>,<página de contas ('Lista de Bens declarados')>,<página das eleições anteriores ('Eleições Anteriores')>
#   Exemplo: /path/to/candidato.html,/path/to/candidato-contas.html,/path/to/candidato-anteriores.html
#
# OUTPUT: um arquivo json onde cada `key` é o nome de um candidato
#
#   Formato: {
#               "NOME DO CANDIDATO": {
#                   "Tipo Informacao 1": "Informacao 1",
#                   "Tipo Informacao 2 ": "Informacao 2"
#            }
#   Exemplo: {
#               "CANDIDATO A": {
#                   "Nome candidato": "CANDIDATO A",
#                   "Nome Urna": "Cand A"
#            }

filename = ARGV[0] || 'files'
output = ARGV[1] || 'candidatos.json'

lines = CSV.open(filename).readlines

def parse_gastos(data)
  keys = [data.css('span').first.text, data.css('span').last.text]
  values = data.css('h3').map(&:text)
  return 'Limite Legal de Gastos', Hash[keys.zip(values)]
end

def parse_multiple_values(data)
  sites = []
  data.css('h3').each do |site|
    sites << site.text.strip unless site.attr('class') == 'ng-hide'
  end
  sites.size == 1 ? sites.first : sites
end

def get_basic_data(data)
  keys = []
  values = []
  data.each do |link|
    if link.children[3]
    title = link.children[3].css('span')
    content = link.children[3].css('h3')
    if title.length > 1 && content.length > 1
      key, value = parse_gastos(link.children[3])
    elsif title.length == 1 && content.length > 1
      key = title.text
      value = parse_multiple_values(link.children[3])
    else
      key = title.text
      value = content.text
    end
    keys << key.strip
    values << (value.is_a?(String) ? value.strip : value)
    end
  end
  Hash[keys.zip(values)]
end

def write_to_file(output, data)
  File.open(output, 'w') do |f|
    f.puts JSON.pretty_generate(data)
  end
  puts "File saved on #{output}"
end

def get_vice_info(doc)
  vice = {}
  img = doc.css(".dvg-vice-container").css('img')
  order = {0 => 'Nome', 1 => 'Cargo', 2 => 'Partido' }
  vice[order[0]] = img.attr('alt').text
  vice['Picture'] = img.attr('src').value
  return 'Vice-Presidente', vice
end

def get_registry_process(doc, candidato)
  doc.css('.dvg-painel-etiqueta-badge .text').each do |e|
    p = e.css('span').map(&:text)
    candidato[p.last] = p.first
  end
end

def get_archives(data)
  archives = {}
  data.css('a').each do |link|
    archives[link.text] = link.attr('href')
  end
  archives
end

def read_file(filename)
  begin
    file = File.read(filename)
    Nokogiri::HTML file.gsub('<!-- <div', '<div').gsub('div> -->', 'div>')
  rescue Exception => e
    puts e.inspect
    nil
  end
end

def get_candidato(doc)
  candidato = get_basic_data(doc.css('.col-md-8').css(".dvg-painel-info-etiqueta"))
  get_registry_process(doc, candidato)
  key, candidato[key] = doc.css('.dvg-table-info').css('td').last.text.split(' - ')
  candidato['Picture'] = doc.css('.dvg-cand-foto').attr('src').value
  candidato['Archives'] = get_archives(doc.css('.dvg-list-arquivos'))
  key, candidato[key] = get_vice_info(doc)
  candidato
end

def get_properties(doc)
  properties = {}
  return properties if doc.nil?
  moeda = 'R$'
  total = doc.css('.dvg-painel-azul')
  value = total.css('.dvg-painel-title').text
  properties[total.css('small').text] = total.css('.dvg-painel-title').text.gsub(moeda, '')
  properties['Moeda'] = moeda

  details = []
  doc.css('.dvg-table tbody tr').each do |row|
    next unless row.css('td').first.attr('class') != "dvg-td-reset-mobile"
    property = {}
    row.css('td').each do |td|
      key = td.attr('data-title')
      value = td.css('span').text
      property[key] = value
  end
  details << property
  end
  properties['Detalhamento dos Bens'] = details
  properties
end

def get_previous(doc)
  return previous if doc.nil?
  details = []
  keys = doc.css('.dvg-table thead th').map(&:text)
  doc.css('.dvg-table tbody tr').each do |row|
    next unless row.css('td').first.attr('class') != "dvg-td-reset-mobile"
    p = {}
    row.css('td').each_with_index do |td, i|
      key = keys[i].strip
      if key == 'Link'
        value = td.css('a').attr('href')
      else
        value = td.text.gsub('-', '').strip
      end
      p[key] = value
    end
    details << p
  end
  details
end

candidatos = {}
lines.each do |fn|
  candidato = get_candidato(read_file(fn[0]))
  candidato['Bens'] = get_properties(read_file(fn[1]))
  candidato['Eleições Anteriores'] = get_previous(read_file(fn[2]))
  candidatos[candidato['Nome Completo']] = candidato
end
write_to_file(output, candidatos)
